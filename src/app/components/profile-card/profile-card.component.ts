import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile-card',
  templateUrl: './profile-card.component.html',
  styleUrls: ['./profile-card.component.scss'],
})
export class ProfileCardComponent implements OnInit {
  initialFollowerImages = [
    '1.jpg',
    '2.jpg',
    '3.jpg',
    '4.jpg',
    '5.jpg',
    '6.jpg',
    '7.jpg',
    '8.jpg',
  ];
  moreFollowerImages = ['10.jpg', '11.jpg', '12.jpg'];
  allFollowerImages = this.initialFollowerImages;
  combinedFollowers = [
    ...this.initialFollowerImages,
    ...this.moreFollowerImages,
  ];
  showAllFollowerImages = false;
  shownTooltip = -1;

  constructor() {}

  ngOnInit(): void {}

  getFollowerPath(imageName: string) {
    return `../../../assets/img/followers/${imageName}`;
  }

  showMoreFollowers() {
    this.showAllFollowerImages = !this.showAllFollowerImages;
    if (this.showAllFollowerImages) {
      this.allFollowerImages = [
        ...this.initialFollowerImages,
        ...this.moreFollowerImages,
      ];
      return;
    }
    this.allFollowerImages = [...this.initialFollowerImages];
  }

  hideTooltip() {
    console.log('HIDE');
    this.shownTooltip = -1;
  }

  showTooltip(index: number) {
    this.shownTooltip = index;
  }

  tooltipIsShown(index: number) {
    return this.shownTooltip == index;
  }
}
