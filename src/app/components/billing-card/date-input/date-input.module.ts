import { NgModule } from '@angular/core';
import { DateInputDirective } from './date-input.directive';

@NgModule({
  declarations: [DateInputDirective],
  exports: [DateInputDirective],
})
export class DateInputModule {}
