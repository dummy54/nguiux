import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appDateInput]',
})
export class DateInputDirective {
  constructor(private elementRef: ElementRef, private renderer: Renderer2) {}

  @HostListener('input', ['$event']) dateInput(event: InputEvent) {
    const element = this.elementRef.nativeElement;
    const value: string = element.value;
    const lastChar = value.slice(-1) === ' ' ? '' : +value.slice(-1);
    const isANumber = typeof lastChar === 'number' && !isNaN(lastChar);
    const formattedAsNumber = value.replace(/[^0-9]*/g, '');
    if (!isANumber && value.length !== 3) {
      this.renderer.setProperty(element, 'value', formattedAsNumber);
    } else if (value.length > 7) {
      this.renderer.setProperty(element, 'value', value.substring(0, 7));
    } else if (value.length === 2 && event.inputType.includes('insert')) {
      const monthValue = value + '/';
      this.renderer.setProperty(element, 'value', monthValue);
    }
  }
}
