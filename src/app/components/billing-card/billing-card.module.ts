import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BillingCardComponent } from './billing-card.component';
import { NumberInputModule } from './number-input/number-input.module';
import { DateInputModule } from './date-input/date-input.module';

@NgModule({
  declarations: [BillingCardComponent],
  imports: [CommonModule, NumberInputModule, DateInputModule],
  exports: [BillingCardComponent],
})
export class BillingCardModule {}
