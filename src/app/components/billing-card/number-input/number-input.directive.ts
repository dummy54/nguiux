import {
  Directive,
  ElementRef,
  HostListener,
  Input,
  Renderer2,
} from '@angular/core';

@Directive({
  selector: '[appNumberInput]',
})
export class NumberInputDirective {
  @Input() limit = 30;

  constructor(private elementRef: ElementRef, private renderer: Renderer2) {}

  @HostListener('input', ['$event']) input(event: Event) {
    const nativeElement = this.elementRef.nativeElement;
    const numberFormatted = nativeElement.value.replace(/[^0-9]*/g, '');
    if (nativeElement !== numberFormatted) {
      const value = numberFormatted.substring(0, this.limit);
      this.renderer.setProperty(nativeElement, 'value', value);
    }
  }
}
