import { Component, OnInit } from '@angular/core';

type MoviePosterImageName = 'joker.jpg' | '1917.jpg' | 'avengers.jpg';

export interface Poster {
  imageName: MoviePosterImageName;
}

@Component({
  selector: 'app-movie-card',
  templateUrl: './movie-card.component.html',
  styleUrls: ['./movie-card.component.scss'],
})
export class MovieCardComponent implements OnInit {
  posters: Poster[] = [
    {
      imageName: 'joker.jpg',
    },
    {
      imageName: '1917.jpg',
    },
    {
      imageName: 'avengers.jpg',
    },
  ];

  constructor() {}

  ngOnInit(): void {}
}
