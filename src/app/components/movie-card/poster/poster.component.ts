import { Component, Input, OnInit } from '@angular/core';
import { Poster } from '../movie-card.component';

@Component({
  selector: 'app-poster',
  templateUrl: './poster.component.html',
  styleUrls: ['./poster.component.scss'],
})
export class PosterComponent implements OnInit {
  @Input() poster: Poster = { imageName: '1917.jpg' };
  @Input() index = 0;

  constructor() {}

  ngOnInit(): void {}

  getTranslateX() {
    let translateXValue = 2;
    switch (this.index) {
      case 0:
        translateXValue = 2;
        break;
      case 2:
        translateXValue = -2;
        break;
      default:
        translateXValue = 0;
    }
    return `translateX(${translateXValue}rem)`;
  }
}
