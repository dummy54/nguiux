import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MovieCardComponent } from './movie-card.component';
import { IconTextComponent } from './icon-text/icon-text.component';
import { PosterComponent } from './poster/poster.component';

@NgModule({
  declarations: [MovieCardComponent, IconTextComponent, PosterComponent],
  imports: [CommonModule],
  exports: [MovieCardComponent],
})
export class MovieCardModule {}
