import { Component, Input, OnInit } from '@angular/core';

type Icon = 'plus' | 'download';

@Component({
  selector: 'app-icon-text',
  templateUrl: './icon-text.component.html',
  styleUrls: ['./icon-text.component.scss'],
})
export class IconTextComponent implements OnInit {
  @Input() icon: Icon = 'plus';

  constructor() {}

  ngOnInit(): void {}
}
