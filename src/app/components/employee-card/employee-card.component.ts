import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-employee-card',
  templateUrl: './employee-card.component.html',
  styleUrls: ['./employee-card.component.scss'],
})
export class EmployeeCardComponent implements OnInit {
  card = {
    employee: {
      name: 'Jane Roe',
      jobDescription: 'UI/UX Designer',
      image: 'woman-smiling.jpeg',
    },
    button: {
      icon: '',
      color: '',
    },
    gradientColor: 'rgb(0, 0, 0)',
  };

  constructor() {}

  ngOnInit(): void {}

  setBackgroundImage() {
    const image = this.card.employee.image;
    const backgroundImage = `url("../../../assets/img/${image}")`;
    return { 'background-image': backgroundImage };
  }

  setGradient() {
    const gradientColor = this.card.gradientColor;
    const background = `linear-gradient(transparent 0, ${gradientColor} 90%)`;
    return { background: background };
  }
}
