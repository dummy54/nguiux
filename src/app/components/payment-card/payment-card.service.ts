import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Game } from './cart-section/cart-section.component';

@Injectable({
  providedIn: 'root',
})
export class PaymentCardService {
  private selectedGames = new BehaviorSubject<Game[]>([]);
  currentSelectedGames = this.selectedGames.asObservable();

  constructor() {}

  addSelectedGames(games: Game[]) {
    this.selectedGames.next(games);
  }
}
