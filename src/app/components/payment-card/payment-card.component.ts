import { Component, OnInit } from '@angular/core';

export type HeaderIcon = {
  hasLeftLine: boolean;
  leftLineActive: boolean;
  isPending: boolean;
  isCurrent: boolean;
  hasRightLine: boolean;
  rightLineActive: boolean;
  heading: string;
};

@Component({
  selector: 'app-payment-card',
  templateUrl: './payment-card.component.html',
  styleUrls: ['./payment-card.component.scss'],
})
export class PaymentCardComponent implements OnInit {
  headerIcons: HeaderIcon[] = [
    {
      hasLeftLine: false,
      leftLineActive: false,
      isPending: true,
      isCurrent: false,
      hasRightLine: true,
      rightLineActive: false,
      heading: 'Cart',
    },
    {
      hasLeftLine: true,
      leftLineActive: false,
      isPending: true,
      isCurrent: false,
      hasRightLine: true,
      rightLineActive: false,
      heading: 'Payment',
    },
    {
      hasLeftLine: true,
      leftLineActive: false,
      isPending: true,
      isCurrent: false,
      hasRightLine: false,
      rightLineActive: false,
      heading: 'Complete',
    },
  ];
  creditCardIcons = [
    'cc-visa-brands.svg',
    'cc-mastercard-brands.svg',
    'cc-discover-brands.svg',
    'cc-amex-brands.svg',
  ];
  currentSectionIndex = 1;

  constructor() {}

  ngOnInit(): void {
    this.setHeaderIconsStyles();
  }

  onChangeSection(sectionIndexToGo: number) {
    this.currentSectionIndex = sectionIndexToGo;
    this.setHeaderIconsStyles();
  }

  setHeaderIconsStyles() {
    this.headerIcons = this.headerIcons.map(
      (icon: HeaderIcon, iconIndex: number) => {
        if (iconIndex === this.currentSectionIndex) {
          icon.isCurrent = true;
          icon.leftLineActive = icon.hasLeftLine;
          icon.rightLineActive = !icon.hasRightLine;
        } else if (iconIndex < this.currentSectionIndex) {
          icon.isPending = false;
          icon.isCurrent = false;
          icon.rightLineActive = icon.hasRightLine;
        } else {
          icon.isCurrent = false;
          icon.isPending = true;
          icon.leftLineActive = !icon.hasLeftLine;
        }
        return icon;
      }
    );
  }
}
