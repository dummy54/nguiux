import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaymentCardComponent } from './payment-card.component';
import { IconTextComponent } from './icon-text/icon-text.component';
import { CartSectionComponent } from './cart-section/cart-section.component';
import { PaymentSectionComponent } from './payment-section/payment-section.component';
import { CompleteSectionComponent } from './complete-section/complete-section.component';
import { ButtonsComponent } from './buttons/buttons.component';
import { ReactiveFormsModule } from '@angular/forms';
import { GamesListComponent } from './games-list/games-list.component';
import { VerifiedComponent } from './verified/verified.component';

@NgModule({
  declarations: [
    PaymentCardComponent,
    IconTextComponent,
    CartSectionComponent,
    PaymentSectionComponent,
    CompleteSectionComponent,
    ButtonsComponent,
    GamesListComponent,
    VerifiedComponent,
  ],
  imports: [CommonModule, ReactiveFormsModule],
  exports: [PaymentCardComponent],
})
export class PaymentCardModule {}
