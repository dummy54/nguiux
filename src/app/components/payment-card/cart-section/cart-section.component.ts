import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { PaymentCardService } from '../payment-card.service';

export type Game = {
  imageName: string;
  title: string;
  description: string;
  price: number;
  color: string;
  isSelected: boolean;
};

@Component({
  selector: 'app-cart-section',
  templateUrl: './cart-section.component.html',
  styleUrls: ['./cart-section.component.scss'],
})
export class CartSectionComponent implements OnInit {
  @Output() payButtonClick: EventEmitter<number> = new EventEmitter<number>();
  games: Game[] = [
    {
      imageName: 'nba2k21.jpeg',
      title: 'NBA 2K21',
      description: 'Limited Edition',
      price: 1000,
      color: '#e81e3f',
      isSelected: false,
    },
    {
      imageName: 'pubg.jpeg',
      title: 'PUBG',
      description: 'Special Release',
      price: 420,
      color: '#B08833',
      isSelected: false,
    },
    {
      imageName: 'fortnite.jpg',
      title: 'Fortnite',
      description: "Collector's Item",
      price: 699,
      color: '#6A69F5',
      isSelected: false,
    },
    {
      imageName: 'gtav.jpeg',
      title: 'GTA - V',
      description: '10th Year Anniversary',
      price: 2499.99,
      color: '#48772C',
      isSelected: false,
    },
    {
      imageName: 'freefire.jpeg',
      title: 'Free Fire',
      description: 'Bonus Pack',
      price: 555,
      color: '#259C8B',
      isSelected: false,
    },

    {
      imageName: 'justdance.jpeg',
      title: 'Just Dance',
      description: 'Special Bundle',
      price: 349,
      color: '#EAA479',
      isSelected: false,
    },
  ];
  gamesSelected: Game[] = [];

  constructor(private paymentCardService: PaymentCardService) {}

  ngOnInit(): void {}

  onPayButtonClick() {
    this.paymentCardService.addSelectedGames(this.gamesSelected);
    this.payButtonClick.emit(1);
  }

  onSelectGames(gamesSelected: Game[]) {
    this.gamesSelected = gamesSelected;
  }
}
