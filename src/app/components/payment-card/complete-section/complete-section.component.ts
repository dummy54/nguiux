import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { Game } from '../cart-section/cart-section.component';
import { PaymentCardService } from '../payment-card.service';

@Component({
  selector: 'app-complete-section',
  templateUrl: './complete-section.component.html',
  styleUrls: ['./complete-section.component.scss'],
})
export class CompleteSectionComponent implements OnInit {
  @Output() changeSectionIndex = new EventEmitter<number>();
  selectedGames!: Game[];
  subscription!: Subscription;

  constructor(private paymentCardService: PaymentCardService) {}

  ngOnInit(): void {
    this.getGames();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getGames() {
    this.subscription = this.paymentCardService.currentSelectedGames.subscribe(
      (games: Game[]) => (this.selectedGames = games)
    );
  }

  onBackButtonClick() {
    this.changeSectionIndex.emit(1);
  }

  async onCompleteButtonClick() {
    this.changeSectionIndex.emit(3);
  }
}
