import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompleteSectionComponent } from './complete-section.component';

describe('CompleteSectionComponent', () => {
  let component: CompleteSectionComponent;
  let fixture: ComponentFixture<CompleteSectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompleteSectionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompleteSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
