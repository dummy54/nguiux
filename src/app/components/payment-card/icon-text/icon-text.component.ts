import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-icon-text',
  templateUrl: './icon-text.component.html',
  styleUrls: ['./icon-text.component.scss'],
})
export class IconTextComponent implements OnInit {
  @Input() hasLeftLine = false;
  @Input() leftLineActive = false;
  @Input() isPending = false;
  @Input() isCurrent = true;
  @Input() index = 1;
  @Input() hasRightLine = false;
  @Input() rightLineActive = false;
  @Input() heading = 'Complete';

  constructor() {}

  ngOnInit(): void {}
}
