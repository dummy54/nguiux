import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { HeaderIcon } from '../payment-card.component';

@Component({
  selector: 'app-payment-section',
  templateUrl: './payment-section.component.html',
  styleUrls: ['./payment-section.component.scss'],
})
export class PaymentSectionComponent implements OnInit {
  @Input() headerIcons: HeaderIcon[] = [];
  @Input() creditCardIcons: string[] = [];
  @Output() backButtonClick = new EventEmitter<number>();
  @Output() reviewOrderButtonClick = new EventEmitter<number>();
  paymentForm = this.formBuilder.group({
    cardholderName: this.formBuilder.control(null),
    cardNumber: this.formBuilder.control(null),
    validity: this.formBuilder.control(null),
    securityCode: this.formBuilder.control(null),
  });

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {}

  getValidators(min: number, max: number) {
    return [
      Validators.required,
      Validators.maxLength(min),
      Validators.minLength(max),
    ];
  }

  onBackButtonClick() {
    this.backButtonClick.emit(0);
  }

  onPaymentFormSubmit() {
    if (this.paymentForm.invalid) {
      return;
    }
    this.reviewOrderButtonClick.emit(2);
  }
}
