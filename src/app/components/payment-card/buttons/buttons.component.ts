import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-buttons',
  templateUrl: './buttons.component.html',
  styleUrls: ['./buttons.component.scss'],
})
export class ButtonsComponent implements OnInit {
  @Input() showBackButton = false;
  @Input() showPayButton = false;
  @Input() showReviewOrderButton = false;
  @Input() showCompleteButton = false;
  @Output() backButtonClick = new EventEmitter<number>();
  @Output() payButtonClick = new EventEmitter<number>();
  @Output() completeButtonClick = new EventEmitter<number>();
  constructor() {}

  ngOnInit(): void {}

  onBackButtonClick() {
    this.backButtonClick.emit();
  }

  onPayButtonClick() {
    this.payButtonClick.emit();
  }

  onCompleteButtonClick() {
    this.completeButtonClick.emit();
  }
}
