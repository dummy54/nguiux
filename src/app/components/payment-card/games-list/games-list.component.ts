import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Game } from '../cart-section/cart-section.component';
import { PaymentCardService } from '../payment-card.service';

@Component({
  selector: 'app-games-list',
  templateUrl: './games-list.component.html',
  styleUrls: ['./games-list.component.scss'],
})
export class GamesListComponent implements OnInit {
  @Input() games: Game[] = [];
  @Input() showCheckbox = true;
  @Input() showTotalsCheckbox = true;
  @Input() showTotals = true;
  @Output() gamesSelectedEmit = new EventEmitter<Game[]>();
  totalPrice = 0;
  totalGamesSelected = 0;
  gamesSelected = [];
  gamesOnCartForm = this.fb.group({});
  toggleSelectAll = false;
  showGamesList = false;
  subscription!: Subscription;

  constructor(
    private fb: FormBuilder,
    private paymentCardService: PaymentCardService
  ) {}

  async ngOnInit() {
    await this.getGamesOnCart();
    this.showGamesList = true;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onSelectChange() {
    this.gamesSelected = this.gamesOnCartForm.value.gamesOnCart.filter(
      (game: Game) => game.isSelected
    );
    this.gamesSelectedEmit.emit(this.gamesSelected);
    this.totalGamesSelected = this.gamesSelected.length;
    this.toggleSelectAll = this.totalGamesSelected === this.games.length;
    this.totalPrice = this.gamesSelected.reduce(
      (total: number, game: Game) => total + game.price,
      0
    );
  }

  async getGamesOnCart() {
    const initialSelected: Game[] = await new Promise((resolve, reject) => {
      this.subscription =
        this.paymentCardService.currentSelectedGames.subscribe(
          (games: Game[]) => resolve(games),
          (error: Error) => reject(error)
        );
      return this.subscription;
    });
    this.gamesOnCartForm = this.fb.group({
      gamesOnCart: this.fb.array(
        this.games.map((game: Game) =>
          this.fb.group({
            ...game,
            isSelected: initialSelected.find(
              (selectedGame: Game) => selectedGame.imageName === game.imageName
            ),
          })
        )
      ),
    });
    this.onSelectChange();
  }

  get gamesOnCart(): FormArray {
    return this.gamesOnCartForm.controls['gamesOnCart'] as FormArray;
  }

  getGameAsFormGroup(index: number): FormGroup {
    return this.gamesOnCart.controls[index] as FormGroup;
  }

  onSelectClick(index: number, game: any) {
    const selected = this.getGameAsFormGroup(index);
    selected.setValue({ ...game.value, isSelected: !game.value.isSelected });
  }

  onToggleSelectAll() {
    this.games.forEach((game: Game, index: number) => {
      const gameControl = this.getGameAsFormGroup(index);
      gameControl.setValue({ ...game, isSelected: !this.toggleSelectAll });
    });
    this.onSelectChange();
  }
}
