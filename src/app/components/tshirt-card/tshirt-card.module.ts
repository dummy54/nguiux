import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TshirtCardComponent } from './tshirt-card.component';

@NgModule({
  declarations: [TshirtCardComponent],
  imports: [CommonModule],
  exports: [TshirtCardComponent],
})
export class TshirtCardModule {}
