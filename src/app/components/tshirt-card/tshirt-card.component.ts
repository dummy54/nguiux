import { Component, OnInit } from '@angular/core';

type HoodieColor = 'red' | 'green' | 'blue';

@Component({
  selector: 'app-tshirt-card',
  templateUrl: './tshirt-card.component.html',
  styleUrls: ['./tshirt-card.component.scss'],
})
export class TshirtCardComponent implements OnInit {
  activeColor: HoodieColor = 'red';
  showImage = true;

  constructor() {}

  ngOnInit(): void {}

  getImagePath(hoodieColor: HoodieColor = this.activeColor) {
    console.log('hoodieColor', hoodieColor);
    return `../../../assets/img/hoodies/${hoodieColor}.jpg`;
  }

  changeImage(hoodieColor: HoodieColor) {
    this.showImage = false;
    this.activeColor = hoodieColor;
    const imagePath = this.getImagePath();
    this.showImage = true;
  }
}
