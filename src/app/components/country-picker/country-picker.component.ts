import { Component, ElementRef, OnInit } from '@angular/core';

type Country = {
  label: string;
  value: string;
};

@Component({
  selector: 'app-country-picker',
  templateUrl: './country-picker.component.html',
  styleUrls: ['./country-picker.component.scss'],
})
export class CountryPickerComponent implements OnInit {
  countries = [
    {
      label: 'Philippines',
      value: 'PH',
    },
    {
      label: 'United States of America',
      value: 'USA',
    },
    {
      label: 'New Zealand',
      value: 'NZ',
    },
    {
      label: 'Hongkong',
      value: 'HK',
    },
    {
      label: 'China',
      value: 'CN',
    },
  ];
  showDropdown = true;
  chosenIndex = -1;
  pickedCountry = '';

  constructor(private elementRef: ElementRef) {}

  ngOnInit(): void {}

  choose(index: number) {
    this.chosenIndex = index;
    this.pickedCountry = this.countries[index].label;
    this.showDropdown = false;
  }

  scrollIntoCountry() {
    const chosen = this.elementRef.nativeElement.querySelector('.chosen');
    if (chosen) {
      chosen?.scrollIntoView({ block: 'nearest', behavior: 'smooth' });
    }
  }

  highlightPickedCountry() {
    this.countries.find((country: Country, index: number) => {
      console.log(this.pickedCountry);
      if (
        country.label.toLowerCase().startsWith(this.pickedCountry.toLowerCase())
      ) {
        this.chosenIndex = index;
        return true;
      }
      return false;
    });
  }

  pickedCountryChanged() {
    if (this.pickedCountry) {
      this.highlightPickedCountry();
      this.scrollIntoCountry();
      return;
    }
    this.chosenIndex = -1;
  }

  toggleShowDropdown() {
    this.showDropdown = !this.showDropdown;
  }
}
