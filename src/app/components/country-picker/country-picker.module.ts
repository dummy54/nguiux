import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountryPickerComponent } from './country-picker.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [CountryPickerComponent],
  imports: [CommonModule, FormsModule],
  exports: [CountryPickerComponent],
})
export class CountryPickerModule {}
