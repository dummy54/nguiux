import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GamePickComponent } from './game-pick.component';



@NgModule({
  declarations: [GamePickComponent],
  imports: [CommonModule],
  exports: [GamePickComponent],
})
export class GamePickModule {}
