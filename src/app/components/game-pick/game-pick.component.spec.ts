import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GamePickComponent } from './game-pick.component';

describe('GamePickComponent', () => {
  let component: GamePickComponent;
  let fixture: ComponentFixture<GamePickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GamePickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GamePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
