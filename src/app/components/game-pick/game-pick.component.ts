import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-game-pick',
  templateUrl: './game-pick.component.html',
  styleUrls: ['./game-pick.component.scss'],
})
export class GamePickComponent implements OnInit {
  games = [
    {
      imageName: 'pubg.jpeg',
      title: 'PUBG',
    },
    {
      imageName: 'fortnite.jpg',
      title: 'Fortnite',
    },
    {
      imageName: 'gtav.jpeg',
      title: 'GTA - V',
    },
    {
      imageName: 'freefire.jpeg',
      title: 'Free Fire',
    },
    {
      imageName: 'nba2k21.jpeg',
      title: 'NBA 2K21',
    },
    {
      imageName: 'justdance.jpeg',
      title: 'Just Dance',
    },
  ];
  chosenIndex = 0;

  constructor() {}

  ngOnInit(): void {}
}
