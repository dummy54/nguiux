import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-plan-card',
  templateUrl: './plan-card.component.html',
  styleUrls: ['./plan-card.component.scss'],
})
export class PlanCardComponent implements OnInit {
  plans = [
    {
      title: 'Basic',
      size: '1',
      unit: 'gb',
      price: '$5',
      validity: '/mo',
    },
    {
      title: 'Business',
      size: '5',
      unit: 'gb',
      price: '$15',
      validity: '/mo',
    },
    {
      title: 'Gold',
      size: '10',
      unit: 'gb',
      price: '$25',
      validity: '/mo',
    },
  ];
  chosenIndex = 1;

  constructor() {}

  ngOnInit(): void {}
}
