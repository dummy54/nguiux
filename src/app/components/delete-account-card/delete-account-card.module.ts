import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeleteAccountCardComponent } from './delete-account-card.component';

@NgModule({
  declarations: [DeleteAccountCardComponent],
  imports: [CommonModule],
  exports: [DeleteAccountCardComponent],
})
export class DeleteAccountCardModule {}
