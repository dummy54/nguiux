import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteAccountCardComponent } from './delete-account-card.component';

describe('DeleteAccountCardComponent', () => {
  let component: DeleteAccountCardComponent;
  let fixture: ComponentFixture<DeleteAccountCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteAccountCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteAccountCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
