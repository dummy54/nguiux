import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-delete-account-card',
  templateUrl: './delete-account-card.component.html',
  styleUrls: ['./delete-account-card.component.scss'],
})
export class DeleteAccountCardComponent implements OnInit {
  showModal = true;

  constructor() {}

  ngOnInit(): void {}
}
