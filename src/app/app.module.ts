import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BillingCardModule } from './components/billing-card/billing-card.module';
import { CountryPickerModule } from './components/country-picker/country-picker.module';
import { DeleteAccountCardModule } from './components/delete-account-card/delete-account-card.module';
import { EmployeeCardModule } from './components/employee-card/employee-card.module';
import { GamePickModule } from './components/game-pick/game-pick.module';
import { LoginCardModule } from './components/login-card/login-card.module';
import { MovieCardModule } from './components/movie-card/movie-card.module';
import { PaymentCardModule } from './components/payment-card/payment-card.module';
import { PlanCardModule } from './components/plan-card/plan-card.module';
import { ProfileCardModule } from './components/profile-card/profile-card.module';
import { TshirtCardModule } from './components/tshirt-card/tshirt-card.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    EmployeeCardModule,
    TshirtCardModule,
    ProfileCardModule,
    GamePickModule,
    BillingCardModule,
    PlanCardModule,
    DeleteAccountCardModule,
    LoginCardModule,
    CountryPickerModule,
    PaymentCardModule,
    MovieCardModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
