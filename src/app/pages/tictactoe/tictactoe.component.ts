import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tictactoe',
  templateUrl: './tictactoe.component.html',
  styleUrls: ['./tictactoe.component.scss'],
})
export class TictactoeComponent implements OnInit {
  winningCombinations = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  squares = ['', '', '', '', '', '', '', '', ''];
  xIndices: number[] = [];
  oIndices: number[] = [];
  letterToBePut = 'X';
  gameEnded = false;
  winner = '';

  constructor() {}

  ngOnInit(): void {}

  occupySquare(index: number) {
    if (!this.squares[index]) {
      this.squares[index] = this.letterToBePut;
      if (this.letterToBePut === 'X') {
        this.xIndices.push(index);
        this.letterToBePut = 'O';
        this.gameEnded = this.checkWinningCombination('X');
        return;
      }
      this.oIndices.push(index);
      this.letterToBePut = 'X';
      this.gameEnded = this.checkWinningCombination('Y');
    }
  }

  isOccupied(index: number) {
    return this.squares[index];
  }

  checkWinningCombination(type: 'X' | 'Y') {
    let toBeChecked = this.oIndices;
    let isWinning = false;
    if (type === 'X') {
      toBeChecked = this.xIndices;
    }
    this.winningCombinations.find((combination: number[]) => {
      isWinning = combination.every((index: number) =>
        toBeChecked.includes(index)
      );
      if (isWinning) {
        this.winner = type;
      }
      return isWinning;
    });
    return isWinning;
  }

  restartGame() {
    this.squares = ['', '', '', '', '', '', '', '', ''];
    this.xIndices = [];
    this.oIndices = [];
    this.letterToBePut = 'X';
    this.gameEnded = false;
    this.winner = '';
  }
}
